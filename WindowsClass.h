// WindowsProcedure.h

#ifndef WINDOWSCLASS_H
#define WINDOWSCLASS_H

#include <Windows.h>

class WindowsClass
{
public:
	WindowsClass();
	WindowsClass(LPWSTR classname, HINSTANCE& hinstance);
	~WindowsClass();
	
	WNDCLASS getWinClass();

	void Register();
	HWND CreateClassWindow(LPWSTR windowname);
	static bool WindowCheckUpdate(HWND& checkhwnd, int show);

	void SetInstance(HINSTANCE& hinstance);
	void SetClassName(LPWSTR classname);
	void SetMenuName(LPWSTR menuname);
	void SetIcon(HICON icon);
	void SetCursor(HCURSOR cursor);
	void SetBackground(HBRUSH background);
	
public:
	WNDCLASS mWinClass;

};

WindowsClass::WindowsClass()
{
	mWinClass.style = CS_HREDRAW | CS_VREDRAW;
	mWinClass.lpfnWndProc = 0;
	mWinClass.cbClsExtra = 0;
	mWinClass.cbWndExtra = 0;
	mWinClass.hInstance = 0;
	mWinClass.hIcon = ::LoadIcon(0, IDI_APPLICATION);
	mWinClass.hCursor = ::LoadCursor(0, IDC_ARROW);
	mWinClass.hbrBackground = (HBRUSH)::GetStockObject(WHITE_BRUSH);
	mWinClass.lpszMenuName = 0;
	mWinClass.lpszClassName = L"";
}

inline WindowsClass::WindowsClass(LPWSTR classname, HINSTANCE & hinstance)
{
	mWinClass.style = CS_HREDRAW | CS_VREDRAW;
	mWinClass.lpfnWndProc = 0;
	mWinClass.cbClsExtra = 0;
	mWinClass.cbWndExtra = 0;
	mWinClass.hInstance = hinstance;
	mWinClass.hIcon = ::LoadIcon(0, IDI_APPLICATION);
	mWinClass.hCursor = ::LoadCursor(0, IDC_ARROW);
	mWinClass.hbrBackground = (HBRUSH)::GetStockObject(WHITE_BRUSH);
	mWinClass.lpszMenuName = 0;
	mWinClass.lpszClassName = classname;
}

WindowsClass::~WindowsClass()
{
}

inline WNDCLASS WindowsClass::getWinClass()
{
	return mWinClass;
}

inline void WindowsClass::Register()
{
	RegisterClass(&mWinClass);
}

inline HWND WindowsClass::CreateClassWindow(LPWSTR windowname)
{

	return CreateWindow( mWinClass.lpszClassName, windowname,
		WS_OVERLAPPEDWINDOW, 0, 0, 500, 500, 0, 0, mWinClass.hInstance, 0);
}

inline bool WindowsClass::WindowCheckUpdate(HWND & checkhwnd, int show)
{
	if (checkhwnd == 0)
	{
		::MessageBox(0, L"CreateWindow - Failed", 0, 0);
		return true;
	}
	else
	{
		ShowWindow(checkhwnd, show);
		UpdateWindow(checkhwnd);

		return false;
	}
		
}


inline void WindowsClass::SetInstance(HINSTANCE & hinstance)
{
	mWinClass.hInstance = hinstance;
}

inline void WindowsClass::SetClassName(LPWSTR classname)
{
	mWinClass.lpszClassName = classname;
}

inline void WindowsClass::SetMenuName(LPWSTR menuname)
{
	mWinClass.lpszMenuName = menuname;
}

inline void WindowsClass::SetIcon(HICON icon)
{
	mWinClass.hIcon = icon;
}

inline void WindowsClass::SetCursor(HCURSOR cursor)
{
	mWinClass.hCursor = cursor;
}

inline void WindowsClass::SetBackground(HBRUSH background)
{
	mWinClass.hbrBackground = background;
}

#endif // !WINDOWSPROCEDURE_H
