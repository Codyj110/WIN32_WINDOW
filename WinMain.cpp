#include <windows.h>
#include "WindowsClass.h"

HWND ghMainWnd = 0;
HWND ghMainWnd2 = 0;
HINSTANCE ghAppInst = 0;

LRESULT CALLBACK
WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch ( msg )
	{
	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE)
			DestroyWindow(hWnd);
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

int WINAPI
WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR cmdLine, int showCmd)
{
	ghAppInst = hInstance;

	WindowsClass wc(L"MyWndClassName", ghAppInst);
	wc.mWinClass.lpfnWndProc = WndProc;

	WindowsClass wc2(L"MyWnd2", ghAppInst);
	wc2.mWinClass.lpfnWndProc = WndProc;
	
	wc.Register();
	wc2.Register();

	ghMainWnd = wc.CreateClassWindow(L"window 1");
	ghMainWnd2 = wc2.CreateClassWindow(L"window 2");
	

	if (WindowsClass::WindowCheckUpdate(ghMainWnd,showCmd))
	{
		return false;
	}

	if (WindowsClass::WindowCheckUpdate(ghMainWnd2, showCmd))
	{
		return false;
	}

	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	while (GetMessage(&msg, 0, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}